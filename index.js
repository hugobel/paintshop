const { baseN } = require('js-combinatorics');
const { createInterface } = require('readline');
const { normalize } = require('./modules/input');
const { createOrderTable, mapCustomer, parseMappingResults } = require('./utils/collection');

// Reads the incoming data
const interface = createInterface({
  input: process.stdin,
  output: process.stdout
});

// Shows generic error message, could be changed to display e.message
const handleError = e => {
  console.error('No solution exists');
};

const getPathVal = (acc, val) => acc + val;

interface.on('line', async (input) => {

  try {
    const {orderSize, customerOrders} = await normalize(input);

    const customers = mapCustomer(customerOrders);

    const slots = createOrderTable(orderSize)(customers);

    const { satisfied, output } = parseMappingResults(slots);

    if (satisfied.size === customers.length) {
      console.log(output);
      return;
    }

    const combinations = baseN([0, 1], output.match(/X/g).length)
      .toArray()
      .sort((a,b) => a.reduce(getPathVal) - b.reduce(getPathVal));

    for (path of combinations) {

      const pathEval = slots.reduce((acc, slot, i) => {
        if(slot.locked) return {
          ...acc,
          output: `${acc.output}${slot.final}`
        };

        const finish = !!path[i] ? 'M' : 'G';

        return {
          output: `${acc.output}${finish}`,
          satisfied: new Set([...acc.satisfied, ...slot[finish]])
        }
      }, { satisfied, output: '' });

      if (pathEval.satisfied.size === customers.length) {
        console.log(pathEval.output);
        return;
      }
    }

    handleError('Could not fins a combination to satisfy the order');

  } catch(e) {
    handleError(e);
  }
});

