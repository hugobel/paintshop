const slotModel = () => ({ G: [], M: [], locked: false });

const createOrderTable = size  => customers => {
  const slots = [];

  customers.forEach((customerColors, customerIndex) => {

    if (customerColors.length < 1) throw new Error('Customer did not select a color');

    if (customerColors.length === 1) { // Color is the sole candidate to fulfill the customer order
      const [colorN, finish] = customerColors[0];
      const pos = colorN-1;

      if (slots[pos] && slots[pos].locked && slots[pos].final !== finish)
        throw new Error('Customers cannot be satisfied');

      if (!slots[pos]) slots[pos] = slotModel(pos);

      slots[pos].locked = true;
      slots[pos].final = finish;
      slots[pos][finish].push(customerIndex);
    } else { // Iterate through every color selected by the customer
      for([colorN, finish] of customerColors) {
        const pos = colorN-1;
        if (pos > size) throw new Error('Selected color is out of range');

        if (!slots[pos]) slots[pos] = slotModel(pos);

        slots[pos][finish].push(customerIndex);
      }
    }

  });

  return slots;
};

const parseMappingResults = slots => slots.reduce((acc, slot) => {
  return slot.locked ? {
    satisfied: new Set([...acc.satisfied, ...(slot[slot.final])]),
    output: acc.output + slot.final
   } : {
     satisfied: acc.satisfied,
     output: acc.output + 'X'
   };
}, { satisfied: [], output: '' });

const mapCustomer = data => data
  .map(customer => customer
    .match(/\d*(\D)/g).map(color => [
      parseInt(color),
      color[color.length-1].toUpperCase()
    ])
  );


module.exports = {
  createOrderTable,
  parseMappingResults,
  mapCustomer
};
