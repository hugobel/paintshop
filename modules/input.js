const normalize = input => new Promise((resolve) => {
  const illegalChars = input.match(/[^(\d|"M"|"G"|\\N)]/gi);

  if (!!illegalChars) throw new Error('Input could not be converted to an order');

  const [colors, ...customerOrders] = input.replace(/\s/g, "").split('\\n')

  resolve({
    customerOrders,
    orderSize: parseInt(colors)
  });
});

module.exports = {
  normalize
};